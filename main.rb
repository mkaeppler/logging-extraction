require_relative 'init'

class MyLogger < Gitlab::JsonLogger
  def self.file_name_noext
    'custom'
  end
end

logger = MyLogger.build
logger.info("test")
