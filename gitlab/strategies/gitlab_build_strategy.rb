module Gitlab
  module Strategy
    module Build
      def build
        Gitlab::SafeRequestStore[cache_key] ||=
          new(full_log_path, level: log_level)
      end

      def cache_key
        'logger:' + full_log_path.to_s
      end

      def full_log_path
        Rails.root.join("log", file_name)
      end
    end
  end  
end
