require 'json'

module Gitlab
  module Strategy
    module Json
      def dump_json(data)
        JSON.generate(data)
      end
    end
  end
end
