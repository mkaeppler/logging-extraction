module Gitlab
  module Strategy
    module Json
      def dump_json(data)
        Gitlab::Json.dump(data)
      end
    end
  end
end
