module Gitlab
  module Strategy
    module Build
      def build
        @instance ||= new(full_log_path, level: log_level)
      end

      def full_log_path
        file_name
      end
    end
  end  
end
