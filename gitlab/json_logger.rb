# frozen_string_literal: true
require 'time'

module Gitlab
  class JsonLogger < ::Gitlab::Logger
    def self.file_name_noext
      raise NotImplementedError
    end

    def format_message(severity, timestamp, progname, message)
      data = default_attributes
      data[:severity] = severity
      data[:time] = timestamp.utc.iso8601(3)
      #TODO: this will work when moved to Labkit
      #data[Labkit::Correlation::CorrelationId::LOG_KEY] = Labkit::Correlation::CorrelationId.current_id

      case message
      when String
        data[:message] = message
      when Hash
        data.merge!(message)
      end

      if !respond_to?(:dump_json)
        raise 'Must provide a JSON serialization strategy'
      else
        dump_json(data) + "\n"
      end
    end

    protected

    def default_attributes
      {}
    end
  end
end
