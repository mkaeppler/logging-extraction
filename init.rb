# Ruby Logger module
require 'logger'

# GitLab Logger module
require_relative 'gitlab/logger'
require_relative 'gitlab/json_logger'

if defined?(Gitlab::Application)
  require_relative 'gitlab/strategies/gitlab_build_strategy'
  require_relative 'gitlab/strategies/gitlab_json_strategy'
else
  require_relative 'gitlab/strategies/ruby_build_strategy'
  require_relative 'gitlab/strategies/ruby_json_strategy'
end

Gitlab::JsonLogger.extend(Gitlab::Strategy::Build)
Gitlab::JsonLogger.include(Gitlab::Strategy::Json)
